import { ApolloClient, InMemoryCache } from '@apollo/client'

/* Initiate the Apollo client and export */
export default new ApolloClient( {
  uri: 'https://api.8base.com/cl9qw0og5005s08ms8le79ymz',
  cache: new InMemoryCache()
} )
