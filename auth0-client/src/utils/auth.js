import { Auth, AUTH_STRATEGIES } from '@8base/auth'

/* Initiate the auth client and export */
export default Auth.createClient(
  {
    strategy: AUTH_STRATEGIES.WEB_AUTH0
  },
  {
    domain: 'dev-8xvu6oa8yzf7x6nl.us.auth0.com',
    clientId: 'n9vgt6meUuAKvkbFjfLBMEG86sixdLsc',
    redirectUri: `${ window.location.origin }/auth/callback`,
    logoutRedirectUri: `${ window.location.origin }/logout`
  }
)
